# ~/ABRÜPT/HILDA NORD/LA MORT DE MARAT OU L'ÉNERGIE DU VIDE/*

La [page de ce livre](https://abrupt.cc/hilda-nord/la-mort-de-marat-ou-lenergie-du-vide/) sur le réseau.

## Sur le livre

*La Mort de Marat* de Jacques-Louis David ne donne pas seulement à voir l'épisode historique de l'assassinat contre-révolutionnaire perpétré par Charlotte Corday, mais il fait aussi le portrait du peuple par le vide. La moitié supérieure du tableau, marquée par la contingence qu'offre à l'imaginaire l'absence, constitue la figure informe et mouvante des masses, dont la puissance ne saurait être saisie par la représentation, qu'elle soit picturale ou politique, sans instituer à dessein une limitation de cette puissance. Le vide devient ainsi l'énergie révolutionnaire de la non-représentation, qui organise la pleine autonomie de la potentialité populaire.

## Sur l'autrice

Ne s'occupe pas d'iconologie, d'herméneutique ou de méréologie. S'occupe du néant qui les nimbe. Quelque part à Berlin. Membre du *Kritische Kreis der Kritik*.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
